#!/bin/bash

#I am an update script for Debian based distros

#updating the packages
sudo apt update

#checking if upgrade is required. script will continue only if there is more than 1 package to upgrade
should_upgrade=$(apt list --upgradable 2>/dev/null | wc -l)

if [ $should_upgrade -gt 1 ]; then

        #listing the upgradable packages
        apt list --upgradable

        #confirm proceeding with the upgrade
        printf 'The packages above can be upgraded. Do you want to upgrade them (Y/n): '
        read -r proceed_upgrade
        if [ "$proceed_upgrade" == "n" ] || [ "$proceed_upgrade" == "N" ]; then
                #upgrade will not be performed. exiting
                echo "Abort. Bye!"
        else
                #starting the upgrade
                echo "Proceeding with the upgrade."
                sudo apt upgrade -y
        fi
fi