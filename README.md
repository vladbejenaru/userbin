USERBIN or `~/bin`

This is (the beginning) of my personal collection of personal bash scripts

Do you want to try them? Then usually their best location is in the `~/bin` folder.

`~/bin` is, if you do not know, the place where one stores under linux his personal scripts and executable files. It generally expands to `/home/your_user_name/bin`

If you do not have your `~/bin` folder you can create it:

```
cd ~
mkdir bin
```


Most of the distributions will include it (after a restart) in their default path and you will be able to launch your collection of scripts from everywhere.

Make sure to mark your scripts as executable:

`chmod -R +x ~/bin`

If you want to be able to run your scripts from everywhere append the following line to the file `~/.bashrc` using your favorite editor (like nano, or vim)
```
export PATH="$HOME/bin:$PATH"
```

... and now you should be able to run your scripts from everywhere...

Enjoy!